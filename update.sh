#!/bin/bash

source configs

DOMAIN="$user.nova.chat"
BRIDGES=( "whatsapp" "facebook" "telegram" "hangouts" "manager" )
SORUBRIDGES=( "slack" "instagram" "twitter" )

# stop all containers
docker stop $(docker ps -a -q) 

# caddy
mv caddy/Caddyfile caddy/Caddyfile.bak
cp caddy/Caddyfile-template caddy/Caddyfile
sed -i 's/$(domain)/'"$DOMAIN"'/g' caddy/Caddyfile

mv docker-compose.yml docker-compose.yml.bak
cp docker-compose-vpstemplate.yml docker-compose.yml
sed -i 's/$(db_password)/'"$DB_PASSWORD"'/g' docker-compose.yml
docker-compose pull

pip3 install wheel 
pip3 install pyyaml

# tulir bridges
for i in "${BRIDGES[@]}"
do
    sudo mv bridges/mautrix-$i/config.yaml bridges/mautrix-$i/config.yaml.bak
    sudo cp bridges/mautrix-$i/config-template.yaml bridges/mautrix-$i/config.yaml
    sudo sed -i 's/$(db_password)/'"$DB_PASSWORD"'/g;s/$(domain)/'"$DOMAIN"'/g;s/$(user)/'"$user"'/g;s/$(sharedSecret)/'"$sharedSecret"'/g' bridges/mautrix-$i/config.yaml
    python3 addToken.py bridges/mautrix-$i/config.yaml.bak bridges/mautrix-$i/config.yaml
    sudo cp outfile.yml bridges/mautrix-$i/config.yaml
    rm outfile.yml
done

# soru bridges
for i in "${SORUBRIDGES[@]}"
do
    sudo mv bridges/mx-puppet-$i/config.yaml bridges/mx-puppet-$i/config.yaml.bak
    sudo cp bridges/mx-puppet-$i/config-template.yaml bridges/mx-puppet-$i/config.yaml
    sudo sed -i 's/$(domain)/'"$DOMAIN"'/g;s/$(user)/'"$user"'/g;s/$(sharedSecret)/'"$sharedSecret"'/g' bridges/mx-puppet-$i/config.yaml
done

sed -i 's/#    restart/    restart/g' docker-compose.yml
docker-compose up -d
