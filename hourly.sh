#!/bin/bash

cd /home/ubuntu/pi-matrix
/usr/local/bin/docker-compose restart mx-puppet-instagram
/usr/local/bin/docker-compose restart mautrix-facebook
/usr/local/bin/docker-compose restart mautrix-hangouts
/usr/local/bin/docker-compose restart mx-puppet-twitter
/usr/local/bin/docker-compose restart matrix-appservice-wechaty