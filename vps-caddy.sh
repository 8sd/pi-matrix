#!/bin/bash


echo "sharedSecret=`pwgen -s 128 1`" >> configs
echo "DB_PASSWORD=`pwgen -s 64 1`" >> configs
source configs

sudo mkdir /srv/web

if [ ! -f docker-compose.yml ]; then
    cp docker-compose-vpstemplate.yml docker-compose.yml
fi


# set up db
if [ ! "$(docker ps | grep postgres)" ]; then
    echo "setting up db"
    sed -i 's/$(db_password)/'"$DB_PASSWORD"'/g' docker-compose.yml
    docker-compose up -d db
fi
echo "db up"

if [ ! -f caddy/Caddyfile ]; then
    cp caddy/Caddyfile-template caddy/Caddyfile
    sed -i 's/$(domain)/'"$user"'\.nova\.chat/g' caddy/Caddyfile
    docker-compose up caddy
    echo "caddy up"
fi
